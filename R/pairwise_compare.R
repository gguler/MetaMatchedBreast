#' A function for returning a list of plots to examine the behaviour of different genes across multiple datasets.
#'
#' This function allows you to subset all of the data frames by a gene of interest, and return the expression values of that gene for every sample and then plot the comparative levels of expression and fold changes of that gene per dataset.
#' #TP == Pre treatment T-On is the first on treatment sample TM is the late on treatment sample TS is the surgical resection or farthest on-treatment samples 4months
#' @param gene the character string of the EntrezgeneID you are interested in.
#' @param T1 the character string of the First time point you are interested in.
#' @param T2 the character string of the Second time point you are interested in.
#' @keywords genes
#' @export
#' @examples
#' pairwise_compare("2", "TP", "TS")


pairwise_compare <- function(gene, T1, T2){

  require(tidyverse)
  require(ggpubr)
  #Find all pairs of samples across T1/T2
  #TP == Pre treatment
  #T-On is the first on treatment sample
  #TM is the late on treatment sample
  #TS is the surgical resection or farthest on-treatment samples 4months +

  #call on subset_gene to extract the necessary information
  dfr <- subset_gene(gene) %>% na.omit()

  #create the annotation matrix
  annot <- do.call(rbind, lapply(MetaMatchedBreast::eset_list, pData))
  fixed_patient <- annot %>% mutate(Patient = ifelse(DS == "E1", paste0(.$Patient.ID, "X"), .$Patient.ID))

  #find the overlap
  First_pair <- fixed_patient %>% dplyr::filter(Biopsy.Time == T1)
  Second_pair <- fixed_patient %>% dplyr::filter(Biopsy.Time == T2)

  #save that overlap
  F_IN_S <- First_pair[which(First_pair$Patient %in% Second_pair$Patient), "Patient"]

  #Create the new annotation matrix with only the required fields from the overlap
  pheno <- fixed_patient %>% filter(Patient %in% F_IN_S & Biopsy.Time == T1 | Biopsy.Time == T2)

  #merge and plot the boxplots
  colnames(dfr)[1] <- "GOI"
  dfr$Sample.ID <- row.names(dfr)
  chec <- merge(dfr, pheno, by = "Sample.ID")
  output1 <- ggplot(chec, aes(x = Biopsy.Time, y = GOI, color = Biopsy.Time)) +
    geom_boxplot() +
    stat_compare_means(comparisons = list(c(T1, T2))) +
    facet_wrap(~DS)
  #plot the fold changes
  output2 <- ggplot(chec, aes(x = Biopsy.Time, y = GOI)) + geom_boxplot() + geom_line(aes(color = DS, group = Patient)) + facet_wrap(~DS)
  output_list <- list(output1, output2)
  output_list
}
