#' A function for returning a list of plots to examine the behaviour of different genes across multiple datasets.
#'
#' This function allows you to subset all of the data frames by a gene of interest, and return the expression values of that gene for every sample and then plot the comparative levels of expression and fold changes of that gene per dataset.
#' #TP == Pre treatment T-On is the first on treatment sample TM is the late on treatment sample TS is the surgical resection or farthest on-treatment samples 4months
#' @param data the character string of the EntrezgeneID you are interested in.
#' @keywords genes
#' @export
#' @examples


add_data <- function(data){

  require(tidyverse)
  require(ggpubr)

  dfr <- MetaMatchedBreast::eset_list
  new_ds_index <- length(dfr) + 1

  dfr[[new_ds_index]] <- data

  saveRDS('data/MetaMatchedBreastData.rda')
}
